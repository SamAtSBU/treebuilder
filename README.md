# TreeBuilder
TreeBuilder is a JavaFX program which can create decision trees based off of CSV data. 

### Usage
Optional Parameters
`[--tree=file --class=class]` to specify default file to load & class attribute to select (must be used together)

### Data Format
The data should be formatted into one CSV file, which contains attribute labels as the first row, with each subsequent row containing a record. TreeBuilder will allow you to choose your class attribute, so the order of labels doesn't matter. 

### For Developers
The ID3 tree building algorithm is included. Additional algorithms can be put in the algorithms package, and must conform to the TreeBuilder interface.