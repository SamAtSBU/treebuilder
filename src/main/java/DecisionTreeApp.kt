import controllers.FindDotViewController
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import controllers.MainViewController

import java.io.IOException
import java.util.*
import javafx.scene.layout.AnchorPane
import javafx.stage.Modality
import utilities.DotCommand


/**
 * Created by sam on 10/25/17.
 */

class DecisionTreeApp : Application() {
    private var params = HashMap<String, String>()
    override fun start(primaryStage: Stage) {
        params.putAll(super.getParameters().named)

        Locale.setDefault(Locale.US)

        primaryStage.title = "Tree Builder"

        if(!checkDot()) {
            showDotView(primaryStage)
        }

        showMainView(primaryStage)
    }

    private fun checkDot(): Boolean {
        try {
            ProcessBuilder(DotCommand.command, "-V").start()
        } catch (e: IOException) {
            println(e.message)
            return false
        }

        return true
    }
    private fun showMainView(primaryStage: Stage) {
        try {
            val loader = FXMLLoader(this.javaClass.getResource("views/MainView.fxml"), ResourceBundle.getBundle("localization/Labels"))
            val parent = loader.load<Parent>()
            primaryStage.scene = Scene(parent)
            primaryStage.minHeight = 760.0
            primaryStage.minWidth = 760.0

            val mainVC = loader.getController<MainViewController>()
            mainVC.closeCallbackFor(primaryStage)

            if (params["file"] != null && params["class"] != null) {
                mainVC.setFile(params["file"]!!)
                mainVC.setClass(params["class"]!!)
            }

            primaryStage.show()
        } catch (e: IOException) {
            System.err.println(e.toString())
        }
    }
    private fun showDotView(primaryStage: Stage) {
        val loader = FXMLLoader(javaClass.getResource("views/FindDot.fxml"))
        val page = loader.load<Any>() as AnchorPane

        // Create the dialog Stage.
        val dialogStage = Stage()
        dialogStage.title = "Choose Dot Location"
        dialogStage.initModality(Modality.WINDOW_MODAL)
        dialogStage.initOwner(primaryStage)
        val scene = Scene(page)
        dialogStage.scene = scene

        // Set the person into the controller.
        //val controller = loader.getController<FindDotViewController>()

        dialogStage.showAndWait()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Application.launch(DecisionTreeApp::class.java, *args)
        }
    }
}

