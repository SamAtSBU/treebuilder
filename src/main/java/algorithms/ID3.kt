package algorithms

import data.Set
import data.Tree

class ID3 : TreeBuilder {
    private var nodeCount = 0
    override fun buildTree(data: Set): Tree {
        return Tree(buildRoot(data))
    }

    private fun buildRoot(data: Set): Tree.Node {
        return when {
            data.records.size == 1 -> Tree.Node.Leaf(data.classAttr, data.getMajority())
            data.attributes.size == 2 -> Tree.Node.Leaf(data.classAttr, data.getMajority())
            data.getEntropy() == 0.0 -> Tree.Node.Leaf(data.classAttr, data.getMajority())
            else -> {
                val (attr, sets) = data.bestSplit()
                val node = Tree.Node(attr)
                sets.forEach { value, set ->
                    node.edges.add(Tree.Edge(value, buildRoot(set)))
                }
                nodeCount++
                return node
            }
        }
    }
}