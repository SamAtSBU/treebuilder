package algorithms

import data.Set
import data.Tree

interface TreeBuilder {
    fun buildTree(data: Set): Tree
}