@file:Suppress("unused")

package controllers

import algorithms.ID3
import data.Set
import data.Tree
import algorithms.TreeBuilder
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.value.ChangeListener
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.geometry.Bounds
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.ZoomEvent
import javafx.stage.FileChooser
import parsers.CSVParser
import parsers.Parser
import java.io.File
import java.io.FileInputStream
import java.net.URL
import java.nio.file.Files
import java.util.*
import javafx.scene.control.Alert.AlertType
import javafx.stage.Stage
import utilities.DotCommand


class MainViewController : Initializable {
    @FXML
    lateinit var rootNode: Node
    @FXML
    lateinit var scrollPane: ScrollPane
    @FXML
    lateinit var treeImageView: ImageView
    @FXML
    lateinit var fileLabel: Label
    @FXML
    lateinit var classPicker: ComboBox<String>
    @FXML
    lateinit var zoomSlider: Slider
    @FXML
    lateinit var zoomLabel: Label
    @FXML
    lateinit var fitHeightButton: Button

    private var data: Set? = null
    private var attributesProperty: ObservableList<String> = FXCollections.observableArrayList<String>()
    private var algorithm: TreeBuilder = ID3()
    private var needsSave = false

    private val fileProperty = SimpleObjectProperty<File>()
    private val imagePathProperty = SimpleStringProperty()
    private val scaleFactorProperty = SimpleDoubleProperty(100.0)


    val tempDir: File = Files.createTempDirectory("Trees_").toFile()

    init {
        Runtime.getRuntime().addShutdownHook(object : Thread() {
            override fun run() {
                tempDir.deleteRecursively()
            }
        })
    }

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        treeImageView.isPreserveRatio = true

        zoomSlider.valueProperty().bindBidirectional(scaleFactorProperty)

        fitHeightButton.disableProperty().bind(zoomSlider.disableProperty())

        scaleFactorProperty.addListener { _, _, newValue ->
            treeImageView.fitHeight = treeImageView.image.height * (newValue.toDouble() / 100)
            zoomLabel.text = "${newValue.toInt()}%"
        }

        fileProperty.addListener { _, _, newValue ->
            val parser: Parser? = when(newValue.extension) {
                "csv" -> CSVParser()
                else -> null
            }

            if(parser != null) {
                fileLabel.text = newValue.absolutePath ?: "No file selected"
                data = parser.loadFile(newValue.absolutePath)
                attributesProperty.clear()
                data?.let {
                    attributesProperty.addAll(it.attributes)
                }
            } else {
                val alert = Alert(AlertType.WARNING)
                alert.title = "Warning"
                alert.headerText = "Invalid File Format"
                alert.contentText = "No parser available for '.${newValue.extension}' files."

                alert.showAndWait()
            }


        }
        classPicker.valueProperty()?.addListener { _, _, newValue ->
            data?.classAttr = newValue
        }

        classPicker.items = attributesProperty

        imagePathProperty.addListener { _, _, newValue ->
            treeImageView.image = Image(FileInputStream(newValue))
            treeImageView.fitHeight = treeImageView.image?.height ?: 0.0
            scaleFactorProperty.value = 100.0
            zoomSlider.isDisable = false
        }
    }

    private fun makeImage(tree: Tree) {
        needsSave = true
        val hash = UUID.randomUUID()
        tree.toGV("${tempDir.absolutePath}${File.separator}$hash.gv")
        val pb = ProcessBuilder(DotCommand.command, "-Tpng", "-o${tempDir.absolutePath}${File.separator}$hash.png", "${tempDir.absolutePath}${File.separator}$hash.gv")
        pb.start().waitFor()
        imagePathProperty.set("${tempDir.absolutePath}${File.separator}$hash.png")
    }

    @FXML
    fun zoomInAction() {
        treeImageView.fitHeight *= 1.25
    }

    @FXML
    fun zoomOutAction() {
        treeImageView.fitHeight *= 0.75
    }

    @FXML
    fun fitHeightAction() {
        setZoom(scrollPane.viewportBounds.height / treeImageView.image.height * 100.0)
    }

    @FXML
    fun fitWidthAction() {
        treeImageView.fitWidth = scrollPane.viewportBounds.width
        treeImageView.fitHeight = 0.0
    }

    @FXML
    fun saveAction(): Boolean {
        val fileChooser = FileChooser()
        fileChooser.extensionFilters.add(FileChooser.ExtensionFilter("Image", "*.png"))
        val file = fileChooser.showSaveDialog(rootNode.scene.window)
        if(file != null) {
            val imageFile = File(imagePathProperty.get())

            imageFile.copyTo(file, true)
            needsSave = false
        }

        return file != null
    }

    @FXML
    fun chooseFile() {
        val fileChooser = FileChooser()
        fileProperty.value = fileChooser.showOpenDialog(rootNode.scene.window)
    }

    @FXML
    fun makeTree() {
        data?.let {
            val tree = algorithm.buildTree(it)
            makeImage(tree)
        }
    }

    private val heightListener = ChangeListener<Bounds>({ _, _, newValue ->
        treeImageView.fitHeight = newValue.height
    })

    private val widthListener = ChangeListener<Bounds>({ _, _, newValue ->
        treeImageView.fitWidth = newValue.width
    })

    @FXML
    fun onZoom(event: ZoomEvent) {
        setZoom(scaleFactorProperty.value * event.zoomFactor)
    }

    fun closeCallbackFor(stage: Stage) {
        stage.setOnCloseRequest {
            if(needsSave) {
                val alert = Alert(AlertType.WARNING)
                alert.title = "Unsaved Changes"
                alert.headerText = "Unsaved Changes"
                alert.contentText = "The tree generated from '${fileProperty.get().absolutePath}' has not been saved. Would you like to save now?"
                alert.buttonTypes.setAll(ButtonType.YES, ButtonType.NO, ButtonType.CANCEL)
                val res = alert.showAndWait()

                when(res.get()) {
                    ButtonType.YES -> {
                        if(!saveAction())
                            it.consume()
                    }
                    ButtonType.CANCEL -> it.consume()
                }
            }
        }
    }

    fun setFile(fileName: String) {
        fileProperty.value = File(fileName)
    }

    fun setClass(className: String) {
        classPicker.valueProperty().value = className
    }

    private fun setZoom(percent: Double) {
        if(!zoomSlider.isDisable && percent >= zoomSlider.min && percent <= zoomSlider.max)
            scaleFactorProperty.value = percent
    }

    @FXML
    private fun debugAction() {
        println(rootNode.boundsInParent)
    }

}