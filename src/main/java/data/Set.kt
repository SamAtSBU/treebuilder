package data

class Set(val records: ArrayList<Record> = ArrayList(), val attributes: List<String>, var classAttr: String = "class") {
    fun getEntropy(): Double {
        var sum = 0.0
        val classCounts: HashMap<String, Int> = HashMap()
        records.forEach { record ->
            classCounts[record.attributes.getOrDefault(this.classAttr, "class")] = (classCounts[record.attributes[this.classAttr]] ?: 0) + 1
        }

        classCounts.keys.forEach { theClass ->
            val px: Double = (classCounts[theClass] ?: 0) / records.size.toDouble()
            sum += -(px * Math.log(px).div(Math.log(2.0)))
        }

        return sum
    }

    private fun splitOn(attr: String): Pair<HashMap<String, Set>, Double> {
        val entr = this.getEntropy()
        val subsets = HashMap<String, Set>()
        records.forEach { record ->
            if (subsets[record.attributes[attr]] == null) {
                subsets[record.attributes[attr]!!] = Set(attributes = this.attributes.filter { it != attr }, classAttr = classAttr)
            }
            subsets[record.attributes[attr]]?.records?.add(record)
        }

        var sum = 0.0
        subsets.keys.forEach { key ->
            val pt = subsets[key]!!.records.size.toDouble() / this.records.size.toDouble()
            sum += pt * subsets[key]!!.getEntropy()
        }

        return Pair(subsets, entr - sum)
    }

    fun bestSplit(): Pair<String, HashMap<String, Set>> {
        var bestSplitData: Pair<String, Double> = Pair("", 0.0)
        var bestSplitSets = HashMap<String, Set>()
        this.attributes.forEach {
            if (it == this.classAttr)
                return@forEach
            val split = this.splitOn(attr = it)
            if (split.second > bestSplitData.second) {
                bestSplitData = Pair(it, split.second)
                bestSplitSets = split.first
            }
        }
        return Pair(bestSplitData.first, bestSplitSets)
    }

    fun getMajority(): String {
        return records.first().attributes[classAttr]!!
    }

    class Record(val attributes: Map<String, String>)
}