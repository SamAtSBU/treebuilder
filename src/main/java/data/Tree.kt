package data

import java.io.FileWriter
import data.Set.Record

@Suppress("unused")
class Tree(private val root: Node) {
    open class Node(open val attr: String) {
        class NullNode : Node("null")
        class Leaf(override val attr: String, val value: String) : Node(attr) {
            override fun toString(): String {
                return "$attr = $value"
            }
        }

        val edges = ArrayList<Edge>()
        fun takeEdgeFor(value: String): Node {
            edges.forEach { edge ->
                if (edge.value == value) {
                    return edge.to
                }
            }
            return NullNode()
        }
    }

    class Edge(val value: String, val to: Node)

    val leaves: Array<Node.Leaf>
        get() {
            val leaves = ArrayList<Node.Leaf>()
            val discovered = ArrayList<Node>()

            fun iter(node: Node) {
                discovered.add(node)
                if (node is Node.Leaf)
                    leaves.add(node)
                node.edges.forEach {
                    if (!discovered.contains(it.to))
                        iter(it.to)
                }
            }

            iter(this.root)

            return leaves.toTypedArray()
        }

    fun getRules() {
        val discovered = ArrayList<Node>()
        val parents = ArrayList<Pair<String, String>>()

        fun iter(node: Node, parents: ArrayList<Pair<String, String>>) {
            discovered.add(node)
            if (node is Node.Leaf) {
                parents.forEachIndexed { idx, it ->
                    print("${it.first}=${it.second}")
                    if (idx < parents.size - 1)
                        print(" AND ")
                }

                print(" THEN ${node.attr}=${node.value} \n")
            }

            node.edges.forEach {
                if (!discovered.contains(it.to)) {
                    parents.add(Pair(node.attr, it.value))
                    iter(it.to, ArrayList(parents))
                    parents.removeAt(parents.size - 1)
                }
            }
        }

        iter(this.root, parents)
    }

    fun classify(record: Record): Pair<String, String> {
        var n = root

        while (n !is Node.Leaf) {
            val value = record.attributes[n.attr] ?: String()
            n = n.takeEdgeFor(value)
        }

        return Pair(n.attr, n.value)
    }

    fun toGV(filename: String) {
        val file = FileWriter(filename)
        val discovered = ArrayList<Node>()

        fun iter(node: Node) {
            discovered.add(node)
            if (node is Node.Leaf) file.write("\tN_${node.hashCode()} [ label = \"${node.attr}=${node.value}\" ];\n")
            else file.write("\tN_${node.hashCode()} [ label = \"${node.attr}\" ];\n")
            node.edges.forEach {
                file.write("\tN_${node.hashCode()} -> N_${it.to.hashCode()} [ label = \"${it.value}\" ];\n")
                if (!discovered.contains(it.to))
                    iter(it.to)
            }
        }

        file.write("digraph data {\n" +
                "\tnode [shape = circle];\n")
        iter(this.root)
        file.write("}")

        file.flush()
        file.close()


    }
}