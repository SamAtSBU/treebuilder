package parsers

import data.Set
import java.io.FileReader

class CSVParser: Parser {
    override fun loadFile(fileName: String): Set {
        val file = FileReader(fileName)
        val lines = file.readLines()
        val records = ArrayList<Set.Record>()
        val headers = ArrayList<String>()

        lines.forEachIndexed { index, line ->
            if (index == 0) {
                headers.addAll(line.split(","))
                return@forEachIndexed
            }
            val attrs = HashMap<String, String>()
            line.split(",").forEachIndexed { index1, value ->
                attrs[headers[index1]] = value
            }
            records.add(Set.Record(attrs))
        }

        return Set(records = records, attributes = headers)
    }
}