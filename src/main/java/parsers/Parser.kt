package parsers

import data.Set

interface Parser {
    fun loadFile(fileName: String): Set
}