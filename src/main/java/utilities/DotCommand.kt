package utilities

object DotCommand {
    var command: String = "dot"
    init {
        if(System.getProperty("os.name").toLowerCase().startsWith("mac")) {
            command = "/usr/local/bin/" + command
        }
        println("Dot: $command")
    }
}